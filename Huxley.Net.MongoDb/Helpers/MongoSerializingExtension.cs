﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace Huxley.Net.MongoDb.Helpers {
    public static class MongoSerializingExtension {
        public static BsonDocument RenderToBsonDocument<T>(this FilterDefinition<T> filter) {
            var serializerRegistry = BsonSerializer.SerializerRegistry;
            var documentSerializer = serializerRegistry.GetSerializer<T>();
            return filter.Render(documentSerializer, serializerRegistry);
        }

        public static BsonDocument RenderToBsonDocument<T>(this SortDefinition<T> sort) {
            var serializerRegistry = BsonSerializer.SerializerRegistry;
            var documentSerializer = serializerRegistry.GetSerializer<T>();
            return sort.Render(documentSerializer, serializerRegistry);
        }
        
        public static string ToJson<T>(this FilterDefinition<T> filter) {
            return filter.RenderToBsonDocument().ToJson();
        }
        
        public static string ToJson<T>(this SortDefinition<T> sort) {
            return sort.RenderToBsonDocument().ToJson();
        }
    }
}