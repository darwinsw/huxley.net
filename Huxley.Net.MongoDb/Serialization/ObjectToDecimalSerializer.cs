﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;

namespace Huxley.Net.MongoDb.Serialization {
    public class ObjectToDecimalSerializer<T> : SerializerBase<T> {
        private readonly Func<decimal, T> valueDeserializer;
        private readonly Func<T, decimal> valueSerializer;

        public ObjectToDecimalSerializer(Func<decimal, T> valueDeserializer, Func<T, decimal> valueSerializer) {
            this.valueDeserializer = valueDeserializer;
            this.valueSerializer = valueSerializer;
        }

        public override T Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args) {
            decimal value = Decimal128.ToDecimal(context.Reader.ReadDecimal128());
            return valueDeserializer(value);
        }

        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, T obj) {
            context.Writer.WriteDecimal128(valueSerializer(obj));
        }
    }
}