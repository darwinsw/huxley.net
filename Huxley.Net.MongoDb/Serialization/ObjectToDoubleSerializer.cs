﻿using System;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;

namespace Huxley.Net.MongoDb.Serialization {
    public class ObjectToDoubleSerializer<T> : SerializerBase<T> {
        private readonly Func<double, T> valueDeserializer;
        private readonly Func<T, double> valueSerializer;

        public ObjectToDoubleSerializer(Func<double, T> valueDeserializer, Func<T, double> valueSerializer) {
            this.valueDeserializer = valueDeserializer;
            this.valueSerializer = valueSerializer;
        }

        public override T Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args) {
            double value = context.Reader.ReadDouble();
            return valueDeserializer(value);
        }

        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, T obj) {
            context.Writer.WriteDouble(valueSerializer(obj));
        }
    }
}