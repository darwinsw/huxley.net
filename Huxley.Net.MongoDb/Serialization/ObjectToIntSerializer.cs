﻿using System;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;

namespace Huxley.Net.MongoDb.Serialization {
    public class ObjectToIntSerializer<T> : SerializerBase<T> {
        private readonly Func<int, T> valueDeserializer;
        private readonly Func<T, int> valueSerializer;

        public ObjectToIntSerializer(Func<int, T> valueDeserializer, Func<T, int> valueSerializer) {
            this.valueDeserializer = valueDeserializer;
            this.valueSerializer = valueSerializer;
        }

        public override T Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args) {
            int value = context.Reader.ReadInt32();
            return valueDeserializer(value);
        }

        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, T obj) {
            context.Writer.WriteInt32(valueSerializer(obj));
        }
    }
}