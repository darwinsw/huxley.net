﻿using System;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;

namespace Huxley.Net.MongoDb.Serialization {
    public class ObjectToStringSerializer<T> : SerializerBase<T> {
        private readonly Func<string, T> valueDeserializer;
        private readonly Func<T, string> valueSerializer;

        public ObjectToStringSerializer(Func<string, T> valueDeserializer, Func<T, string> valueSerializer) {
            this.valueDeserializer = valueDeserializer;
            this.valueSerializer = valueSerializer;
        }

        public override T Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args) {
            string value = context.Reader.ReadString();
            return valueDeserializer(value);
        }

        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, T obj) {
            context.Writer.WriteString(valueSerializer(obj));
        }
    }
}