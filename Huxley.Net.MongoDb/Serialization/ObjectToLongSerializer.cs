﻿using System;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;

namespace Huxley.Net.MongoDb.Serialization {
    public class ObjectToLongSerializer<T> : SerializerBase<T> {
        private readonly Func<long, T> valueDeserializer;
        private readonly Func<T, long> valueSerializer;

        public ObjectToLongSerializer(Func<long, T> valueDeserializer, Func<T, long> valueSerializer) {
            this.valueDeserializer = valueDeserializer;
            this.valueSerializer = valueSerializer;
        }

        public override T Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args) {
            long value = context.Reader.ReadInt64();
            return valueDeserializer(value);
        }

        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, T obj) {
            context.Writer.WriteInt64(valueSerializer(obj));
        }
    }
}