﻿using System.Threading.Tasks;
using MongoDB.Driver;

namespace Huxley.Net.MongoDb.Driver {
    public static class CollectionExtension {
        private static FilterDefinition<T> EmptyFilter<T>() {
            return Builders<T>.Filter.Empty;
        }

        public static Task DeleteAllAsync<T>(this IMongoCollection<T> collection) {
            return collection.DeleteManyAsync(EmptyFilter<T>());
        }
        
        public static Task<IAsyncCursor<T>> FindAllAsync<T>(this IMongoCollection<T> collection) {
            return collection.FindAsync(EmptyFilter<T>());
        }
        
        public static Task<long> CountAllAsync<T>(this IMongoCollection<T> collection) {
            return collection.CountAsync(EmptyFilter<T>());
        }
    }
}