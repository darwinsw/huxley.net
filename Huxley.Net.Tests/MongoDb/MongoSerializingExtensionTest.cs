﻿using Huxley.Net.MongoDb.Helpers;
using MongoDB.Driver;
using NUnit.Framework;

namespace Huxley.Net.Tests.MongoDb {
    [TestFixture]
    public class MongoSerializingExtensionTest {
        [Test]
        public void JsonizeSingleFieldFilterDefinition() {
            FilterDefinition<Fixture> filter = Builders<Fixture>.Filter.Eq(x => x.IntVal, 19);
            string serialized = filter.ToJson();
            Assert.That(serialized, Is.EqualTo("{ \"IntVal\" : 19 }"));
        }
        
        [Test]
        public void JsonizeSingleFieldSortDefinition() {
            SortDefinition<Fixture> filter = Builders<Fixture>.Sort.Ascending(x => x.IntVal);
            string serialized = filter.ToJson();
            Assert.That(serialized, Is.EqualTo("{ \"IntVal\" : 1 }"));
        }
        
        [Test]
        public void JsonizeAndMultiFieldFilterDefinition() {
            FilterDefinition<Fixture> filter = Builders<Fixture>.Filter.And(
                Builders<Fixture>.Filter.Eq(x => x.IntVal, 19),
                Builders<Fixture>.Filter.Eq(x => x.NestedVal.IntVal, 11)
            );
            string serialized = filter.ToJson();
            Assert.That(serialized, Is.EqualTo("{ \"IntVal\" : 19, \"NestedVal.IntVal\" : 11 }"));
        }
        
        [Test]
        public void JsonizeMultiFieldSortDefinition() {
            SortDefinition<Fixture> filter = Builders<Fixture>.Sort.Ascending(x => x.IntVal).Descending(x => x.NestedVal.StringVal);
            string serialized = filter.ToJson();
            Assert.That(serialized, Is.EqualTo("{ \"IntVal\" : 1, \"NestedVal.StringVal\" : -1 }"));
        }
        
        [Test]
        public void JsonizeOrMultiFieldFilterDefinition() {
            FilterDefinition<Fixture> filter = Builders<Fixture>.Filter.Or(
                Builders<Fixture>.Filter.Eq(x => x.IntVal, 19),
                Builders<Fixture>.Filter.Eq(x => x.NestedVal.IntVal, 11)
            );
            string serialized = filter.ToJson();
            Assert.That(serialized, Is.EqualTo("{ \"$or\" : [{ \"IntVal\" : 19 }, { \"NestedVal.IntVal\" : 11 }] }"));
        }
    }

    internal class Fixture {
        public int IntVal { get; set; }
        public Nested NestedVal { get; set; }
    }

    internal class Nested {
        public string StringVal { get; set; }
        public int IntVal { get; set; }
    }
}