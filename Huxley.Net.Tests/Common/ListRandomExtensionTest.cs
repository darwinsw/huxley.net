﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Huxley.Net.Common;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace Huxley.Net.Tests.Common {
    [TestFixture]
    public class ListRandomExtensionTest {
        [Test]
        public void ExtractedItemComesFromProvidedList() {
            string[] items = {"a", "b", "c", "d", "e"};
            string extracted = items.Random();
            Assert.That(items.Contains(extracted));
        }

        [Test]
        public void ThrowsExceptionWhenItemsListIsNull() {
            int[] items = null;
            Assert.That(() => items.Random(), Throws.TypeOf<ArgumentException>());
        }

        [Test]
        public void ThrowsExceptionWhenItemsListIsEmpty() {
            int[] items = { };
            Assert.That(() => items.Random(), Throws.TypeOf<ArgumentException>());
        }

        [Test]
        public void ExtractedItemDistributionIsQuiteUniform() {
            string[] items = {"a", "b", "c", "d", "e"};
            IDictionary<string, int> frequency = items.ToDictionary(x => x, x => 0);
            const int totalCount = 10000;
            for (int i = 0; i < totalCount; i++) {
                string extracted = items.Random();
                frequency[extracted] = frequency[extracted] + 1;
            }
            const double delta = 0.20;
            Assert.That(frequency.Count(x => x.Value < (1 - delta) * (((double)totalCount) / items.Length)), Is.Zero);
            Assert.That(frequency.Count(x => x.Value > (1 + delta) * (((double)totalCount) / items.Length)), Is.Zero);
        }
    }
}