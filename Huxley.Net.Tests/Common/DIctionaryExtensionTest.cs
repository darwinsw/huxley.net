﻿using System.Collections.Generic;
using Huxley.Net.Common;
using NUnit.Framework;

namespace Huxley.Net.Tests.Common {
    [TestFixture]
    public class DIctionaryExtensionTest {
        [Test]
        public void GetExistingValueFromDictionary() {
            IDictionary<string, string> dictionary = new Dictionary<string, string> {{"key", "value"}};
            string actual = dictionary.GetOrDefault("key", "defaultValue");
            Assert.That(actual, Is.EqualTo("value"));
        }
        
        [Test]
        public void GetNotExistingValueFromDictionaryWithDefault() {
            IDictionary<string, string> dictionary = new Dictionary<string, string> {{"key", "value"}};
            string actual = dictionary.GetOrDefault("otherKey", "defaultValue");
            Assert.That(actual, Is.EqualTo("defaultValue"));
        }
    }
}