﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using Huxley.Net.Common.Configuration;
using NUnit.Framework;

namespace Huxley.Net.Tests.Common.Configuration {
    [TestFixture]
    public class PerMachineConfigurationExtensionTest {
        private const string TheKey = "TkeKey";
        private const string TheValue = "TheValue";
        private const string ThePerMachineValue = "ThePerMachineValue";
        
        [Test]
        public void ReadSettingWithoutMachinePrefix() {
            NameValueCollection config = new NameValueCollection {[TheKey] = TheValue};
            string read = config.ReadPerMachineSetting(TheKey);
            Assert.That(read, Is.EqualTo(TheValue));
        }
        
        [Test]
        public void ReadSettingWithMachinePrefixDefaultPrefixMode() {
            string machineName = Environment.MachineName;
            NameValueCollection config = new NameValueCollection {[TheKey] = TheValue, [$"{machineName}.{TheKey}"] = ThePerMachineValue};
            string read = config.ReadPerMachineSetting(TheKey);
            Assert.That(read, Is.EqualTo(ThePerMachineValue));
        }
        
        [Test]
        public void ReadSettingWithMachinePrefixExplicitPrefixMode() {
            string machineName = Environment.MachineName;
            NameValueCollection config = new NameValueCollection {[TheKey] = TheValue, [$"{machineName}.{TheKey}"] = ThePerMachineValue};
            string read = config.ReadPerMachineSetting(TheKey, PerMachineConfigurationMode.MachineNameAsPrefix);
            Assert.That(read, Is.EqualTo(ThePerMachineValue));
        }
        
        [Test]
        public void ReadSettingWithMachinePrefixExplicitSuffixMode() {
            string machineName = Environment.MachineName;
            NameValueCollection config = new NameValueCollection {[TheKey] = TheValue, [$"{TheKey}.{machineName}"] = ThePerMachineValue};
            string read = config.ReadPerMachineSetting(TheKey, PerMachineConfigurationMode.MachineNameAsSuffix);
            Assert.That(read, Is.EqualTo(ThePerMachineValue));
        }
        
        [Test]
        public void ReadConnectionStringWithoutMachinePrefix() {
            ConnectionStringSettingsCollection config =
                new ConnectionStringSettingsCollection {new ConnectionStringSettings(TheKey, TheValue)};

            string read = config.ReadPerMachineConnectionString(TheKey);
            Assert.That(read, Is.EqualTo(TheValue));
        }
        
        [Test]
        public void ReadConnectionStringWithMachinePrefixDefaultPrefixMode() {
            string machineName = Environment.MachineName;
            ConnectionStringSettingsCollection config =
                new ConnectionStringSettingsCollection {
                    new ConnectionStringSettings(TheKey, TheValue),
                    new ConnectionStringSettings($"{machineName}.{TheKey}", ThePerMachineValue)
                };
            string read = config.ReadPerMachineConnectionString(TheKey);
            Assert.That(read, Is.EqualTo(ThePerMachineValue));
        }
        
        [Test]
        public void ReadConnectionStringWithMachinePrefixExplicitPrefixMode() {
            string machineName = Environment.MachineName;
            ConnectionStringSettingsCollection config =
                new ConnectionStringSettingsCollection {
                    new ConnectionStringSettings(TheKey, TheValue),
                    new ConnectionStringSettings($"{machineName}.{TheKey}", ThePerMachineValue)
                };
            string read = config.ReadPerMachineConnectionString(TheKey, PerMachineConfigurationMode.MachineNameAsPrefix);
            Assert.That(read, Is.EqualTo(ThePerMachineValue));
        }
        
        [Test]
        public void ReadConnectionStringWithMachinePrefixExplicitSuffixMode() {
            string machineName = Environment.MachineName;
            ConnectionStringSettingsCollection config =
                new ConnectionStringSettingsCollection {
                    new ConnectionStringSettings(TheKey, TheValue),
                    new ConnectionStringSettings($"{TheKey}.{machineName}", ThePerMachineValue)
                };
            string read = config.ReadPerMachineConnectionString(TheKey, PerMachineConfigurationMode.MachineNameAsSuffix);
            Assert.That(read, Is.EqualTo(ThePerMachineValue));
        }

        [Test]
        public void SuffixModeIsTheSameAsItself() {
            Assert.That(PerMachineConfigurationMode.MachineNameAsSuffix, Is.EqualTo(PerMachineConfigurationMode.MachineNameAsSuffix));
        }
        
        [Test]
        public void PrefixModeIsTheSameAsItself() {
            Assert.That(PerMachineConfigurationMode.MachineNameAsPrefix, Is.EqualTo(PerMachineConfigurationMode.MachineNameAsPrefix));
        }
        
        [Test]
        public void PrefixModeAndSuffixModeAreDifferent() {
            Assert.That(PerMachineConfigurationMode.MachineNameAsPrefix, Is.Not.EqualTo(PerMachineConfigurationMode.MachineNameAsSuffix));
        }
    }
}