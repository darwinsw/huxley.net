﻿using Huxley.Net.Common;
using NUnit.Framework;

namespace Huxley.Net.Tests.Common {
    [TestFixture]
    public class FluentDefaultExtensionTest {
        [Test]
        public void UseDefaultValueWhenCurrentObjectIsNull() {
            object obj = NullValue();
            object actual = obj.Or("defaultValue");
            Assert.That(actual, Is.EqualTo("defaultValue"));
        }

        [Test]
        public void UseActualIntValueWhenItIsNotNull() {
            int value = 19;
            int actual = value.Or(20);
            Assert.That(actual, Is.EqualTo(value));
        }
        
        [Test]
        public void UseActualStringValueWhenItIsNotNull() {
            string value = "abc";
            string actual = value.Or("def");
            Assert.That(actual, Is.EqualTo(value));
        }

        [Test]
        public void UseEmptyStringWhenCurrentIsNull() {
            string value = null;
            string actual = value.OrEmpty();
            Assert.That(actual, Is.EqualTo(string.Empty));
        }
        
        [Test]
        public void UseCurrentStringWhenItINotNull() {
            string value = null;
            string actual = value.OrEmpty();
            Assert.That(actual, Is.EqualTo(string.Empty));
        }

        private static object NullValue() {
            return null;
        }
    }
}