﻿using NUnit.Framework;
using Huxley.Net.Common;

namespace Huxley.Net.Tests.Common {
    [TestFixture]
    public class StringExtensionTest {
        [Test]
        public void IsNullOrEmptyIsTrueWhenInputIsNull() {
            string value = null;
            Assert.That(value.IsNullOrEmpty(), Is.True);
        }
        
        [Test]
        public void IsNullOrEmptyIsTrueWhenInputIsEmpty() {
            string value = string.Empty;
            Assert.That(value.IsNullOrEmpty(), Is.True);
        }
        
        [Test]
        public void IsNullOrEmptyIsFalseWhenInputIsNotEmpty() {
            string value = "not empty";
            Assert.That(value.IsNullOrEmpty(), Is.False);
        }
        
        [Test]
        public void IsNullOrEmptyIsFalseWhenInputContainsOnlyWhitespaces() {
            string value = "          ";
            Assert.That(value.IsNullOrEmpty(), Is.False);
        }
        
        [Test]
        public void IsNullOrWhiteSpaceIsTrueWhenInputIsNull() {
            string value = null;
            Assert.That(value.IsNullOrWhiteSpace(), Is.True);
        }
        
        [Test]
        public void IsNullOrWhiteSpaceIsTrueWhenInputIsEmpty() {
            string value = string.Empty;
            Assert.That(value.IsNullOrWhiteSpace(), Is.True);
        }
        
        [Test]
        public void IsNullOrWhiteSpaceIsFalseWhenInputIsNotEmpty() {
            string value = "not empty";
            Assert.That(value.IsNullOrWhiteSpace(), Is.False);
        }
        
        [Test]
        public void IsNullOrWhiteSpaceIsTrueWhenInputContainsOnlyWhitespaces() {
            string value = "          ";
            Assert.That(value.IsNullOrWhiteSpace(), Is.True);
        }
    }
}