﻿using System;
using System.Data;

namespace Huxley.Net.Sql.DataReader {
    public static class DataReaderExtension {
        public static string GetString(this IDataReader reader, string key) {
            return reader[key] as string;
        }
        
        public static int GetInt(this IDataReader reader, string key) {
            return (int)reader[key];
        }
        
        public static long GetLong(this IDataReader reader, string key) {
            return (long)reader[key];
        }
        
        public static byte GetByte(this IDataReader reader, string key) {
            return reader.GetByte(reader.GetOrdinal(key));
        }
        
        public static bool GetBool(this IDataReader reader, string key) {
            return (bool)reader[key];
        }
        
        public static short GetShort(this IDataReader reader, string key) {
            return (short)reader[key];
        }
        
        public static decimal GetDecimal(this IDataReader reader, string key) {
            return (decimal)reader[key];
        }
        
        public static DateTimeOffset GetDateTimeOffset(this IDataReader reader, string key) {
            DateTimeOffset? dto = reader[key] as DateTimeOffset?;
            if (dto == null) {
                DateTime? dt = reader[key] as DateTime?;
                if (dt != null) {
                    dto = DateTime.SpecifyKind(dt.Value, DateTimeKind.Utc);
                }
            }
            if (dto.HasValue) {
                return dto.Value;
            }
            throw new InvalidCastException();
        }
        
        public static DateTime GetDateTime(this IDataReader reader, string key) {
            return (DateTime)reader[key];
        }
        
        public static Guid GetGuid(this IDataReader reader, string key) {
            Guid? guid = reader[key] as Guid?;
            if (!guid.HasValue) {
                string guidVal = reader[key] as string;
                if (guidVal != null) {
                    guid = Guid.Parse(guidVal);
                }
            }
            if (guid.HasValue) {
                return guid.Value;
            }
            throw new InvalidCastException();
        }
    }
}