﻿using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace Huxley.Net.Sql.Template {
    public class QueryTemplate {
        private readonly IDbConnection connection;

        public QueryTemplate(IDbConnection connection) {
            this.connection = connection;
        }

        public IEnumerable<T> Query<T>(string sql, RowMapper<T> mapper, object parameters = null) {
            ICollection<T> result = new List<T>();
            using (IDbCommand cmd = CreateCommand(sql, parameters)) {
                using (IDataReader reader = cmd.ExecuteReader()) {
                    while (reader.Read()) {
                        result.Add(mapper(reader));
                    }
                }
                return result;
            }
        }

        public T QuerySingleOrDefault<T>(string sql, RowMapper<T> mapper, object parameters = null) {
            T result = default(T);
            using (IDbCommand cmd = CreateCommand(sql, parameters)) {
                using (IDataReader reader = cmd.ExecuteReader()) {
                    if (reader.Read()) {
                        result = mapper(reader);
                    }
                }
            }
            return result;
        }

        private IDbCommand CreateCommand(string sql, object parameters) {
            IDbCommand cmd = connection.CreateCommand();
            cmd.CommandText = sql;
            if (parameters != null) {
                foreach (PropertyInfo prop in parameters.GetType().GetProperties()) {
                    string pName = prop.Name;
                    object pValue = prop.GetValue(parameters);
                    IDbDataParameter parameter = cmd.CreateParameter();
                    parameter.ParameterName = $"@{pName}";
                    parameter.Value = pValue;
                    cmd.Parameters.Add(parameter);
                }
            }
            return cmd;
        }
    }
}