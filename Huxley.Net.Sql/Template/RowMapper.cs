﻿using System.Data;

namespace Huxley.Net.Sql.Template {
    public delegate T RowMapper<out T>(IDataReader reader);
}