﻿using System;
using System.Data;
using System.Data.SqlClient;
using Huxley.Net.SqlIntegrationTests.Sql;
using NUnit.Framework;

namespace Huxley.Net.SqlServerIntegrationTests.Sql {
    [TestFixture]
    public class SqlServerDataReaderExtensionTest : DataReaderExtensionTest {
        protected override string GetConnectionString() {
            return SqlServerConnectionHelper.ConnectionString;
        }

        protected override IDbConnection BuildConnection(string connectionString) {
            return new SqlConnection(connectionString);
        }

        protected override string GetCreateTableSql(string tableName) {
            return $@"create table {tableName} (
                LongVal bigint,
                IntVal int,
                ByteVal tinyint,
                BoolVal bit,
                ShortVal smallint,
                DecimalVal decimal(5, 2),
                DateTimeOffsetVal datetimeoffset,
                DateTimeVal datetime,
                StringVal varchar(64),
                GuidVal uniqueidentifier
            )";
        }
    }
}