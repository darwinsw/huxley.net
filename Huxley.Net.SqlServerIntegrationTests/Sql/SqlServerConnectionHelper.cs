﻿using System;

namespace Huxley.Net.SqlServerIntegrationTests.Sql {
    public static class SqlServerConnectionHelper {
        private const string DefaultConnectionString = "server=localhost;database=tempdb;user id=sa;password=VeryStrongAndSecretPwd123!";
        
        public static string ConnectionString =>  Environment.GetEnvironmentVariable("INTEGRATION_SQL_SERVER_CONNECTION_STRING") ?? DefaultConnectionString;
    }
}