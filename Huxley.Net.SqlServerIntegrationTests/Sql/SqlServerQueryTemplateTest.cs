﻿using System;
using System.Data;
using System.Data.SqlClient;
using Huxley.Net.SqlIntegrationTests.Sql;
using NUnit.Framework;

namespace Huxley.Net.SqlServerIntegrationTests.Sql {
    [TestFixture]
    public class SqlServerQueryTemplateTest : QueryTemplateTest {
        protected override string GetConnectionString() {
            return SqlServerConnectionHelper.ConnectionString;
        }

        protected override IDbConnection OpenConnection(string connectionString) {
            return new SqlConnection(connectionString);
        }

        protected override string GetCreateTableSql(string tableName) {
            return $"create table {tableName} ([Id] int not null primary key, [Name] varchar(16) not null unique, [Value] varchar(64) not null)";
        }
    }
}