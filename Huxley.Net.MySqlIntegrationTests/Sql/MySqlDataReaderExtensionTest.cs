﻿using System;
using System.Data;
using Huxley.Net.SqlIntegrationTests.Sql;
using MySql.Data.MySqlClient;
using NUnit.Framework;

namespace Huxley.Net.MySqlIntegrationTests.Sql {
    [TestFixture]
    public class MySqlDataReaderExtensionTest : DataReaderExtensionTest {
        protected override string GetConnectionString() {
            return MySqlConnectionHelper.ConnectionString;
        }

        protected override IDbConnection BuildConnection(string connectionString) {
            return new MySqlConnection(connectionString);
        }
        
        protected override object CreateValue(DateTimeOffset dto) {
            return dto.ToUniversalTime().DateTime;
        }

        protected override string GetCreateTableSql(string tableName) {
            return $@"create table {tableName} (
                LongVal bigint,
                IntVal int,
                ByteVal tinyint unsigned,
                BoolVal boolean,
                ShortVal smallint,
                DecimalVal decimal(5, 2),
                DateTimeOffsetVal datetime,
                DateTimeVal datetime,
                StringVal varchar(64),
                GuidVal char(38)
            )";
        }
    }
}