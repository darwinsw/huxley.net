﻿using System;
using System.Data;
using Huxley.Net.SqlIntegrationTests.Sql;
using MySql.Data.MySqlClient;
using NUnit.Framework;

namespace Huxley.Net.MySqlIntegrationTests.Sql {
    [TestFixture]
    public class MySqlQueryTemplateTest : QueryTemplateTest {
        protected override string GetConnectionString() {
            return MySqlConnectionHelper.ConnectionString;
        }

        protected override IDbConnection OpenConnection(string connectionString) {
            return new MySqlConnection(connectionString);
        }


        protected override string GetCreateTableSql(string tableName) {
            return $@"create table {tableName} (
                Id INT(6) UNSIGNED,
                Name VARCHAR(16) NOT NULL unique,
                Value VARCHAR(64) NOT NULL,
                PRIMARY KEY(Id)
            )";
        }
    }
}