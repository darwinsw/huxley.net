﻿using System;

namespace Huxley.Net.MySqlIntegrationTests.Sql {
    public static class MySqlConnectionHelper {
        private const string DefaultConnectionString = "Server=localhost;Database=mysql;UID=root;Password=VeryStrongAndSecretPwd123!";
        
        public static string ConnectionString => Environment.GetEnvironmentVariable("INTEGRATION_MYSQL_CONNECTION_STRING") ?? DefaultConnectionString;
    }
}