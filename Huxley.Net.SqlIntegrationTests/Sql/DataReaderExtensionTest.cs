﻿using System;
using System.Data;
using Huxley.Net.Sql.DataReader;
using NUnit.Framework;

namespace Huxley.Net.SqlIntegrationTests.Sql {
    public abstract class DataReaderExtensionTest {
        protected abstract string GetConnectionString();

        protected abstract IDbConnection BuildConnection(string connectionString);
        
        protected abstract string GetCreateTableSql(string tableName);

        private const string TableName = "DataReaderTestData";
        
        private IDbConnection connection;
        private Guid guid;
        private const string TheString = "TestValue";
        private const long TheLong = 11L;
        private const int TheInt = 19;
        private const byte TheByte= 17;
        private const bool TheBool = true;
        private const short TheShort = 22;
        private const decimal TheDecimal = 3.14m;
        private static readonly DateTimeOffset TheDateTimeOffset = new DateTimeOffset(1978, 1, 1, 0, 0, 0, TimeSpan.FromHours(1));
        private static readonly DateTime TheDateTime = new DateTime(1978, 1, 1, 0, 0, 0, DateTimeKind.Unspecified);
        
        [SetUp]
        public void InitTableAndData() {
            connection = BuildConnection(GetConnectionString());
            connection.Open();
            using (IDbCommand cmd = connection.CreateCommand()) {
                cmd.CommandText = $"drop table if exists {TableName}";
                cmd.ExecuteNonQuery();
            }
            guid = Guid.NewGuid();
            using (IDbCommand cmd = connection.CreateCommand()) {
                cmd.CommandText = GetCreateTableSql(TableName);
                cmd.ExecuteNonQuery();
                cmd.CommandText = $"insert into {TableName} values (@l, @i, @by, @bo, @sh, @de, @dto, @dt, @s, @g)";
                cmd.Parameters.Add(CreateParameter(cmd, "l", TheLong));
                cmd.Parameters.Add(CreateParameter(cmd, "i", TheInt));
                cmd.Parameters.Add(CreateParameter(cmd, "by", TheByte));
                cmd.Parameters.Add(CreateParameter(cmd, "bo", TheBool));
                cmd.Parameters.Add(CreateParameter(cmd, "sh", TheShort));
                cmd.Parameters.Add(CreateParameter(cmd, "de", TheDecimal));
                cmd.Parameters.Add(CreateParameter(cmd, "dto", CreateValue(TheDateTimeOffset)));
                cmd.Parameters.Add(CreateParameter(cmd, "dt", TheDateTime));
                cmd.Parameters.Add(CreateParameter(cmd, "s", TheString));
                cmd.Parameters.Add(CreateParameter(cmd, "g", guid));
                cmd.ExecuteNonQuery();
            }
        }

        protected virtual object CreateValue(DateTimeOffset dto) {
            return dto;
        }

        private IDbDataParameter CreateParameter(IDbCommand cmd, string pName, object pValue) {
            IDbDataParameter param = cmd.CreateParameter();
            param.ParameterName = $"@{pName}";
            param.Value = pValue;
            return param;
        }

        [TearDown]
        public void DisposeConnection() {
            connection.Dispose();
        }

        [Test]
        public void ReadStringByName() {
            IDbCommand cmd = connection.CreateCommand();
            cmd.CommandText = $"select * from {TableName}";
            IDataReader reader = cmd.ExecuteReader();
            reader.Read();
            string result = reader.GetString("StringVal");
            Assert.That(result, Is.EqualTo(TheString));
        }
        
        [Test]
        public void ReadIntByName() {
            IDbCommand cmd = connection.CreateCommand();
            cmd.CommandText = $"select * from {TableName}";
            IDataReader reader = cmd.ExecuteReader();
            reader.Read();
            int result = reader.GetInt("IntVal");
            Assert.That(result, Is.EqualTo(TheInt));
        }
        
        [Test]
        public void ReadLongByName() {
            IDbCommand cmd = connection.CreateCommand();
            cmd.CommandText = $"select * from {TableName}";
            IDataReader reader = cmd.ExecuteReader();
            reader.Read();
            long result = reader.GetLong("LongVal");
            Assert.That(result, Is.EqualTo(TheLong));
        }
        
        [Test]
        public void ReadByteByName() {
            IDbCommand cmd = connection.CreateCommand();
            cmd.CommandText = $"select * from {TableName}";
            IDataReader reader = cmd.ExecuteReader();
            reader.Read();
            byte result = reader.GetByte("ByteVal");
            Assert.That(result, Is.EqualTo(TheByte));
        }
        
        [Test]
        public void ReadBoolByName() {
            IDbCommand cmd = connection.CreateCommand();
            cmd.CommandText = $"select * from {TableName}";
            IDataReader reader = cmd.ExecuteReader();
            reader.Read();
            bool result = reader.GetBool("BoolVal");
            Assert.That(result, Is.EqualTo(TheBool));
        }
        
        [Test]
        public void ReadShortByName() {
            IDbCommand cmd = connection.CreateCommand();
            cmd.CommandText = $"select * from {TableName}";
            IDataReader reader = cmd.ExecuteReader();
            reader.Read();
            short result = reader.GetShort("ShortVal");
            Assert.That(result, Is.EqualTo(TheShort));
        }
        
        [Test]
        public void ReadDecimalByName() {
            IDbCommand cmd = connection.CreateCommand();
            cmd.CommandText = $"select * from {TableName}";
            IDataReader reader = cmd.ExecuteReader();
            reader.Read();
            decimal result = reader.GetDecimal("DecimalVal");
            Assert.That(result, Is.EqualTo(TheDecimal));
        }
        
        [Test]
        public void ReadDateTimeOffsetByName() {
            IDbCommand cmd = connection.CreateCommand();
            cmd.CommandText = $"select * from {TableName}";
            IDataReader reader = cmd.ExecuteReader();
            reader.Read();
            DateTimeOffset result = reader.GetDateTimeOffset("DateTimeOffsetVal");
            Assert.That(result, Is.EqualTo(TheDateTimeOffset));
        }
        
        [Test]
        public void ReadDateTimeByName() {
            IDbCommand cmd = connection.CreateCommand();
            cmd.CommandText = $"select * from {TableName}";
            IDataReader reader = cmd.ExecuteReader();
            reader.Read();
            DateTime result = reader.GetDateTime("DateTimeVal");
            Assert.That(result, Is.EqualTo(TheDateTime));
        }
        
        [Test]
        public void ReadGuidByName() {
            IDbCommand cmd = connection.CreateCommand();
            cmd.CommandText = $"select * from {TableName}";
            IDataReader reader = cmd.ExecuteReader();
            reader.Read();
            Guid result = reader.GetGuid("GuidVal");
            Assert.That(result, Is.EqualTo(guid));
        }
    }
}