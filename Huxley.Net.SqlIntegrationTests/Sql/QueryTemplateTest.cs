﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Huxley.Net.Sql.Template;
using NUnit.Framework;

namespace Huxley.Net.SqlIntegrationTests.Sql {
    public abstract class QueryTemplateTest {
        protected abstract string GetConnectionString();

        protected abstract IDbConnection OpenConnection(string connectionString);

        protected abstract string GetCreateTableSql(string tableName);

        private IDbConnection connection;
        
        private const string TableName = "QueryTemplateTestData";

        [Test]
        public void ReadCollectionUsingQueryTemplate() {
            QueryTemplate tpl = new QueryTemplate(connection);
            IEnumerable<SomeData> dataItems = tpl.Query($"select * from {TableName}", MapRow);
            Assert.That(dataItems.Count(), Is.EqualTo(3));
            Assert.That(dataItems, Contains.Item(new SomeData(1, "x", "aaaaaa")));
            Assert.That(dataItems, Contains.Item(new SomeData(2, "y", "bbbbbb")));
            Assert.That(dataItems, Contains.Item(new SomeData(3, "z", "cccccc")));
        }
        
        [Test]
        public void ReadCollectionUsingQueryTemplateWithParameters() {
            QueryTemplate tpl = new QueryTemplate(connection);
            IEnumerable<SomeData> dataItems = tpl.Query($"select * from {TableName} where Name = @K", MapRow, new { K = "y" });
            Assert.That(dataItems.Count(), Is.EqualTo(1));
            Assert.That(dataItems, Contains.Item(new SomeData(2, "y", "bbbbbb")));
        }
        
        [Test]
        public void ReadEmptyCollectionUsingQueryTemplateWithParameters() {
            QueryTemplate tpl = new QueryTemplate(connection);
            IEnumerable<SomeData> dataItems = tpl.Query($"select * from {TableName} where Name = @K", MapRow, new { K = "aa" });
            Assert.That(dataItems.Count(), Is.EqualTo(0));
        }
        
        [Test]
        public void ReadSingleUsingQueryTemplateWithParameters() {
            QueryTemplate tpl = new QueryTemplate(connection);
            SomeData data = tpl.QuerySingleOrDefault($"select * from {TableName} where Name = @K", MapRow, new { K = "y" });
            Assert.That(data, Is.EqualTo(new SomeData(2, "y", "bbbbbb")));
        }
        
        [Test]
        public void ReadNullUsingQueryTemplateWithParameters() {
            QueryTemplate tpl = new QueryTemplate(connection);
            SomeData data = tpl.QuerySingleOrDefault($"select * from {TableName} where Name  = @K", MapRow, new { K = "aa" });
            Assert.That(data, Is.Null);
        }

        [TearDown]
        public void DropTableAndDisposeConnection() {
            using (IDbCommand cmd = connection.CreateCommand()) {
                cmd.CommandText = $"drop table if exists {TableName}";
                cmd.ExecuteNonQuery();
            }
            connection.Dispose();
        }

        private static SomeData MapRow(IDataReader reader) {
            int id = reader.GetInt32(0);
            string key = reader.GetString(1);
            string value = reader.GetString(2);
            return new SomeData(id, key, value);
        }

        [SetUp]
        public void InitTableAndData() {
            connection = OpenConnection(GetConnectionString());
            connection.Open();
            using (IDbCommand cmd = connection.CreateCommand()) {
                cmd.CommandText = GetCreateTableSql(TableName);
                cmd.ExecuteNonQuery();
                cmd.CommandText =
                    $"insert into {TableName} values (1, 'x', 'aaaaaa'), (2, 'y', 'bbbbbb'), (3, 'z', 'cccccc')";
                cmd.ExecuteNonQuery();
            }
        }
    }

    internal class SomeData {
        public SomeData(int id, string name, string value) {
            Id = id;
            Name = name;
            Value = value;
        }

        public int Id { get; }
        public string Name { get; }
        public string Value { get; }

        public override bool Equals(object obj) {
            SomeData that = obj as SomeData;
            return that != null && Id.Equals(that.Id) && Name.Equals(that.Name) && Value.Equals(that.Value);
        }
    }
}