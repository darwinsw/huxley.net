﻿using System;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Huxley.Net.MongoDbIntegrationTests {
    public class DatabaseHelper {
        private static string GetConnectionString() {
            return Environment.GetEnvironmentVariable("INTEGRATION_MONGODB_URI") ?? "mongodb://localhost:27017";
        }

        private static IMongoDatabase GetTestDatabase() {
            var client = new MongoClient(GetConnectionString());
            var db = client.GetDatabase("huxley_test");
            return db;
        }

        public static IMongoCollection<BsonDocument> GetCollection(string collectionName) {
            return GetTestDatabase().GetCollection<BsonDocument>(collectionName);
        }
        
        public static IMongoCollection<T> GetCollection<T>(string collectionName) {
            return GetTestDatabase().GetCollection<T>(collectionName);
        }
    }
}