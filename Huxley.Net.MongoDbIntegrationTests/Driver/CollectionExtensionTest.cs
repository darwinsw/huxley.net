﻿using System.Threading.Tasks;
using Huxley.Net.MongoDb.Driver;
using Huxley.Net.MongoDbIntegrationTests;
using MongoDB.Bson;
using MongoDB.Driver;
using NUnit.Framework;

namespace Huxley.Net.MongoDbIntegrationTests.Driver {
    public class CollectionExtensionTest {
        [Test]
        public async Task FindAllAsync() {
            var coll = DatabaseHelper.GetCollection("myCollection");
            await coll.DeleteAllAsync();
            const int count = 5;
            for (int i = 0; i < count; i++) {
                await coll.InsertOneAsync(new BsonDocument {{"x", i * 1}, {"y", i * 2 + 1}});
            }
            var all = await coll.FindAllAsync();
            Assert.That((await all.ToListAsync()).Count, Is.EqualTo(count));
        }
        
        [Test]
        public async Task CountAllAsync() {
            var coll = DatabaseHelper.GetCollection("myCollection");
            await coll.DeleteAllAsync();
            const int count = 5;
            for (int i = 0; i < count; i++) {
                await coll.InsertOneAsync(new BsonDocument {{"x", i * 1}, {"y", i * 2 + 1}});
            }
            long countAll = await coll.CountAllAsync();
            Assert.That(countAll, Is.EqualTo(count));
        }
    }
}