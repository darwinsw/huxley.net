﻿using Huxley.Net.MongoDb.Serialization;
using Huxley.Net.MongoDbIntegrationTests.Support;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using NUnit.Framework;

namespace Huxley.Net.MongoDbIntegrationTests.Serialization {
    [TestFixture]
    public class ObjectToDecimalSerializerTest : WrapperSerializerTest<decimal> {
        static ObjectToDecimalSerializerTest() {
            BsonClassMap.RegisterClassMap<Container<decimal>>(cm => {
                cm.MapProperty(x => x.WrapperValue).SetSerializer(new ObjectToDecimalSerializer<Wrapper<decimal>>(
                    i => new Wrapper<decimal>(i),  w => w.WrappedValue));
            });
        }

        private const decimal TestValue = 11.19m;

        protected override decimal WrappedValue { get; } = TestValue;
        
        protected override BsonValue WrappedBsonValue { get; } = TestValue;
    }
}