﻿using System.Threading.Tasks;
using Huxley.Net.MongoDb.Driver;
using Huxley.Net.MongoDbIntegrationTests.Support;
using MongoDB.Bson;
using MongoDB.Driver;
using NUnit.Framework;

namespace Huxley.Net.MongoDbIntegrationTests.Serialization {
    public abstract class WrapperSerializerTest<T> {
        [Test]
        public async Task ReadFromDb() {
            var coll = DatabaseHelper.GetCollection("container");
            await coll.DeleteAllAsync();
            await coll.InsertOneAsync(new BsonDocument {{"WrapperValue", WrappedBsonValue}});
            var typedColl = DatabaseHelper.GetCollection<Container<T>>("container");
            Container<T> read = await(await typedColl.FindAsync(Builders<Container<T>>.Filter.Empty)).FirstAsync();
            Assert.That(read.WrapperValue.WrappedValue, Is.EqualTo(WrappedValue));
        }
        
        [Test]
        public async Task WriteToDb() {
            var coll = DatabaseHelper.GetCollection("container");
            await coll.DeleteAllAsync();
            var typedColl = DatabaseHelper.GetCollection<Container<T>>("container");
            
            await typedColl.InsertOneAsync(new Container<T> {
                WrapperValue = new Wrapper<T>(WrappedValue)
            });

            BsonDocument read = await (await coll.FindAsync(Builders<BsonDocument>.Filter.Empty)).FirstAsync();
            
            Assert.That(read["WrapperValue"].AsBsonValue, Is.EqualTo(WrappedBsonValue));
        }

        protected abstract T WrappedValue { get; }

        protected abstract BsonValue WrappedBsonValue { get; }
    }
}