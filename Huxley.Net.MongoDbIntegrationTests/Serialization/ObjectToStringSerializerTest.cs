﻿using Huxley.Net.MongoDb.Serialization;
using Huxley.Net.MongoDbIntegrationTests.Support;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using NUnit.Framework;

namespace Huxley.Net.MongoDbIntegrationTests.Serialization {
    [TestFixture]
    public class ObjectToStringSerializerTest : WrapperSerializerTest<string> {
        static ObjectToStringSerializerTest() {
            BsonClassMap.RegisterClassMap<Container<string>>(cm => {
                cm.MapProperty(x => x.WrapperValue).SetSerializer(new ObjectToStringSerializer<Wrapper<string>>(
                    s => new Wrapper<string>(s), w => w.WrappedValue));
            });
        }

        private const string TestValue = "xyz";

        protected override string WrappedValue { get; } = TestValue;
        
        protected override BsonValue WrappedBsonValue { get; } = TestValue;
    }
}