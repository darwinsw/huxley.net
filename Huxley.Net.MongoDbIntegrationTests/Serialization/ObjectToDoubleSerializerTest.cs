﻿using Huxley.Net.MongoDb.Serialization;
using Huxley.Net.MongoDbIntegrationTests.Support;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using NUnit.Framework;

namespace Huxley.Net.MongoDbIntegrationTests.Serialization {
    [TestFixture]
    public class ObjectToDoubleSerializerTest : WrapperSerializerTest<double> {
        static ObjectToDoubleSerializerTest() {
            BsonClassMap.RegisterClassMap<Container<double>>(cm => {
                cm.MapProperty(x => x.WrapperValue).SetSerializer(new ObjectToDoubleSerializer<Wrapper<double>>(
                    i => new Wrapper<double>(i),  w => w.WrappedValue));
            });
        }

        private const double TestValue = 11.19;

        protected override double WrappedValue { get; } = TestValue;
        
        protected override BsonValue WrappedBsonValue { get; } = TestValue;
    }
}