﻿using Huxley.Net.MongoDb.Serialization;
using Huxley.Net.MongoDbIntegrationTests.Support;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using NUnit.Framework;

namespace Huxley.Net.MongoDbIntegrationTests.Serialization {
    [TestFixture]
    public class ObjectToIntSerializerTest : WrapperSerializerTest<int> {
        static ObjectToIntSerializerTest() {
            BsonClassMap.RegisterClassMap<Container<int>>(cm => {
                cm.MapProperty(x => x.WrapperValue).SetSerializer(new ObjectToIntSerializer<Wrapper<int>>(
                    i => new Wrapper<int>(i),  w => w.WrappedValue));
            });
        }

        private const int TestValue = 11;

        protected override int WrappedValue { get; } = TestValue;
        
        protected override BsonValue WrappedBsonValue { get; } = TestValue;
    }
}