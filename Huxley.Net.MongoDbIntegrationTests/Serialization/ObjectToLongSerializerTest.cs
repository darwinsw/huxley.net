﻿using Huxley.Net.MongoDb.Serialization;
using Huxley.Net.MongoDbIntegrationTests.Support;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using NUnit.Framework;

namespace Huxley.Net.MongoDbIntegrationTests.Serialization {
    [TestFixture]
    public class ObjectToLongSerializerTest : WrapperSerializerTest<long> {
        static ObjectToLongSerializerTest() {
            BsonClassMap.RegisterClassMap<Container<long>>(cm => {
                cm.MapProperty(x => x.WrapperValue).SetSerializer(new ObjectToLongSerializer<Wrapper<long>>(
                    i => new Wrapper<long>(i),  w => w.WrappedValue));
            });
        }

        private const long TestValue = 11;

        protected override long WrappedValue { get; } = TestValue;
        
        protected override BsonValue WrappedBsonValue { get; } = TestValue;
    }
}