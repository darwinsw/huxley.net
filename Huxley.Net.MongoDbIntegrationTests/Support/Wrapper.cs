﻿namespace Huxley.Net.MongoDbIntegrationTests.Support {
    internal class Wrapper<T> {
        public Wrapper(T wrappedValue) {
            WrappedValue = wrappedValue;
        }

        public T WrappedValue { get; }
    }
}