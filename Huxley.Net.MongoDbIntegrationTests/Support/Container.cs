﻿namespace Huxley.Net.MongoDbIntegrationTests.Support {
    internal class Container<T> {
        public Wrapper<T> WrapperValue { get; set; }
    }
}