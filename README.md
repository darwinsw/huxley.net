# Huxley.Net #

| Branch | Pipeline Status |Code Coverage |
| ------------- |:--------------|---|
| master      | [![pipeline status](https://gitlab.com/darwinsw/huxley.net/badges/master/pipeline.svg)](https://gitlab.com/darwinsw/huxley.net/commits/master) | [![coverage](https://gitlab.com/darwinsw/huxley.net/badges/master/coverage.svg)](https://darwinsw.gitlab.io/huxley.net/coverage/master/html-report/index.htm)|
| release | [![pipeline status](https://gitlab.com/darwinsw/huxley.net/badges/release/pipeline.svg)](https://gitlab.com/darwinsw/huxley.net/commits/release) | [![coverage](https://gitlab.com/darwinsw/huxley.net/badges/release/coverage.svg)](https://darwinsw.gitlab.io/huxley.net/coverage/release/html-report/index.htm)|
| ci-dev | [![pipeline status](https://gitlab.com/darwinsw/huxley.net/badges/ci-dev/pipeline.svg)](https://gitlab.com/darwinsw/huxley.net/commits/ci-dev) | [![coverage](https://gitlab.com/darwinsw/huxley.net/badges/ci-dev/coverage.svg)](https://darwinsw.gitlab.io/huxley.net/coverage/ci-dev/html-report/index.htm)|

# Summary

A personal .Net class library, in order to favour cross-project code reuse.

# Table of contents
1. [Development Environment](#development-environment)
    1. [Vagrant](#vagrant)
	2. [Docker Compose](#docker-compose)
	3. [Custom middleware installation](#custom-middleware-installation)
2. [Examples](#examples)
    * [`QueryTemplate` and `RowMapper`](#querytemplate-and-rowmapper)
	* [MongoDb serializers](#mongodb-serializers)
	* [MongoDb extensions] (#mongodb-extensions)

# Development environment

## Vagrant

If you want to use [Vagrant](https://www.vagrantup.com) as middleware provisioning tool, you find a ready-to-use `Vagrantfile` in `<REPOSITORY_ROOT>/provisioning`: launch `vagrant up` in the `provisioning` directory in order to start both MongoDb and SqlServer, preconfigured for integration tests execution.

## Docker (compose)

If you want to use [Docker](https://www.docker.com/) as middleware provisioning tool, you find a ready-to-use `docker-commpose.yml` in `<REPOSITORY_ROOT>/provisioning`: launch `docker-compose up` in the `provisioning` directory in order to start both MongoDb and SqlServer, preconfigured for integration tests execution.

## Custom middleware installation

In order to run the integration tests suite in your local development environment using your own middleware installations, you need

- MongoDb instance responding on `localhost:27017`, without authentication
- SqlServer instance responding on `localhost:1433`, using `MyNotSoStrongP4ssw0rd!321` password for `sa` user

If you don't like these defaults, you can change them providing MongoDb connection string and SqlServer connection string as environment variables, `INTEGRATION_MONGODB_URI` and `INTEGRATION_SQL_SERVER_CONNECTION_STRING` respectively.

Moreover, this approch is adopted in CI/CD pipelines, providing a different hostname for both MongoDb and SqlServer services, in order to match services configuration in [`.gitlab-ci.yml`](https://gitlab.com/darwinsw/huxley.net/blob/master/.gitlab-ci.yml)

# Examples
## QueryTemplate and RowMapper
Given a SQL table like
```
create table [Sample] ( [Id] int, [Key] varchar(32) not null, [Value] varchar(128) not null, [UpdatedAt] datetimeoffset)

insert into Sample values (1, 'x', 'The value of x', '2018-01-01 00:00:00.000 +01:00')
insert into Sample values (2, 'y', 'The value of y', '2018-01-01 01:00:00.000 +01:00')
insert into Sample values (3, 'z', 'The value of z', '2018-01-01 02:00:00.000 +01:00')
```
you can use `QueryTemplate` as follows:
```
IDbConnection connection = .... ;
QueryTemplate tpl = new QueryTemplate(connection);
IEnumerable<SomeData> items = tpl.Query("select * from [Sample] where [Key] <> @K", MapRow, new { K = "y" });
```
where `SomeData` is a user-defined class and `MapRow` is a method compliant to `Huxley.Net.Sql.Template.RowMapper` delegate's signature:
```
class SomeData {
	public SomeData(int id, string key, string value, DateTimeOffset when) {
		Id = id;
		Key = key;
		Value = value;
		When = when;
	}

	public int Id { get; }
	public string Key { get; }
	public string Value { get; }
	public DateTimeOffset When  { get; }
}

// public delegate T RowMapper<out T>(IDataReader reader);

private static SomeData MapRow(IDataReader reader) {
	int id = reader.GetInt32(0);
	string key = reader.GetString(1);
	string value = reader.GetString(2);
	DateTimeOffset when = ((SqlDataReader)reader).GetString(3);
	return new SomeData(id, key, value, when);
}

```

`items` will be something like
```
{
	new SomeData(1, "x", "The value of x"), DateTimeOffset.Parse("2018-01-01T00:00:00.000+01:00"),
	new SomeData(3, "z", "The value of z"), DateTimeOffset.Parse("2018-01-01T00:02:00.000+01:00")
}
```

## MongoDb serializers
Given a class
```
class EmailAddress {
	public EmailAddress(string value) {
		Value = value;
	}
	public string Value { get; }
}
```
or a class
```
class PersonId {
	public PersonId(int value) {
		Value = value;
	}
	
	public int Value { get; }
}
```
you can serialize document fields of type `EmailAddress` or `PersonId` flattening nested `Value` fields:

```
class Person {
	public PersonId Id { gset; set; }
	public string FullName { get; set; }
	public EmailAddress Email { get; set; }
}
```
can be serialized as
```
{
	_id: 1,
	FullName: "Eddy Merckx",
	Email: "eddy@merckx.com"
}
```
using *custom MongoDb serializers* provided by namespace `Huxley.Net.MongoDb.Serialization`, registering them through
MongoDb Driver's API:
```
BsonClassMap.RegisterClassMap<Person>(cm => {
	cm.MapIdMember(x => x.Id).SetSerializer(new ObjectToIntSerializer<PersonId>(
		s => new PersonId(s), id => id.Value));
	cm.MapProperty(x => x.Email).SetSerializer(new ObjectToStringSerializer<EmailAddress>(
		s => new EmailAddress(s), e => e.Value));
});

```

## MongoDb extensions
```
const string connectionString = "...";
// T parameter type

IMongoCollection<T> collection = new MongoClient(connectionString).GetDatabase("a_database")
	.GetCollection<T>(collectionName);
	
IAsyncCursor<T> allItems = await collection.FindAllAsync();

count itemsCount = collection.CountAllAsync();

collection.DeleteAllAsync();
```
