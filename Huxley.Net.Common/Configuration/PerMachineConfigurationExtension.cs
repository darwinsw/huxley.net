﻿using System.Collections.Specialized;
using System.Configuration;

namespace Huxley.Net.Common.Configuration {
    public static class PerMachineConfigurationExtension {
        public static string ReadPerMachineSetting(this NameValueCollection self, string key, PerMachineConfigurationMode mode) {
            return self[mode.BuildPerMachineKey(key)] ?? self[key];
        }

        public static string ReadPerMachineSetting(this NameValueCollection self, string key) {
            return self.ReadPerMachineSetting(key, PerMachineConfigurationMode.MachineNameAsPrefix);
        }

        public static string ReadPerMachineConnectionString(this ConnectionStringSettingsCollection self, string key, PerMachineConfigurationMode mode) {
            return self[mode.BuildPerMachineKey(key)]?.ConnectionString ?? self[key]?.ConnectionString;
        }
        
        public static string ReadPerMachineConnectionString(this ConnectionStringSettingsCollection self, string key) {
            return self.ReadPerMachineConnectionString(key, PerMachineConfigurationMode.MachineNameAsPrefix);
        }
    }
}