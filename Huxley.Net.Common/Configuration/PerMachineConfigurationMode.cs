﻿using System;

namespace Huxley.Net.Common.Configuration {
    public abstract class PerMachineConfigurationMode {
        public abstract string BuildPerMachineKey(string key);
        
        public static PerMachineConfigurationMode MachineNameAsPrefix = new MachineNameAsPrefixMode();
        
        public static PerMachineConfigurationMode MachineNameAsSuffix = new MachineNameAsSuffixMode();
    }

    public class MachineNameAsPrefixMode : PerMachineConfigurationMode {
        public override string BuildPerMachineKey(string key) {
            return $"{Environment.MachineName}.{key}";
        }
    }

    public class MachineNameAsSuffixMode : PerMachineConfigurationMode {
        public override string BuildPerMachineKey(string key) {
            return $"{key}.{Environment.MachineName}";
        }
    }
}