﻿namespace Huxley.Net.Common {
    public static class FluentDefaultExtension {
        public static T Or<T>(this T value, T defaultValue) {
            return value != null ? value : defaultValue;
        }
        
        public static string OrEmpty(this string value) {
            return value.Or(string.Empty);
        }
    }
}