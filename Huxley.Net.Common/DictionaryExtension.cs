﻿using System;
using System.Collections.Generic;

namespace Huxley.Net.Common {
    public static class DictionaryExtension {
        public static TOutput GetOrDefault<TOutput, TValue>(this IDictionary<string, TValue> self, string key, TOutput defaultValue)
            where TOutput : TValue {
            return self.ContainsKey(key) ? (TOutput)self[key] : defaultValue;
        }
    }
}