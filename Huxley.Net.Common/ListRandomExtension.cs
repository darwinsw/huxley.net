﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Huxley.Net.Common {
    public static class ListRandomExtension {
        private static readonly Random Rnd = new Random();
        
        public static T Random<T>(this IList<T> items) {
            if (items == null || !items.Any()) {
                throw new ArgumentException("Can't extract random item from null list");
            }
            return items[Rnd.Next(items.Count)];
        }
    }
}