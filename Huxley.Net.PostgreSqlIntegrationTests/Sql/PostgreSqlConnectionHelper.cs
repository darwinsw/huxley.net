﻿using System;

namespace Huxley.Net.PostgreSqlIntegrationTests.Sql {
    public static class PostgreSqlConnectionHelper {
        private const string DefaultConnectionString = "Server=localhost;Port=5432;User Id=postgres;Password=VeryStrongAndSecretPwd123!;Database=postgres;";

        public static string ConnectionString =>Environment.GetEnvironmentVariable("INTEGRATION_POSTGRES_CONNECTION_STRING") ?? DefaultConnectionString;
    }
}