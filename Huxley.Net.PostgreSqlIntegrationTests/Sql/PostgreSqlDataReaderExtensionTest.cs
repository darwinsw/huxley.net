﻿using System;
using System.Data;
using Huxley.Net.SqlIntegrationTests.Sql;
using Npgsql;

namespace Huxley.Net.PostgreSqlIntegrationTests.Sql {
    public class PostgreSqlDataReaderExtensionTest : DataReaderExtensionTest {
        protected override string GetConnectionString() {
            return PostgreSqlConnectionHelper.ConnectionString;
        }

        protected override IDbConnection BuildConnection(string connectionString) {
            return new NpgsqlConnection(connectionString);
        }

        protected override object CreateValue(DateTimeOffset dto) {
            return dto.ToUniversalTime().DateTime;
        }

        protected override string GetCreateTableSql(string tableName) {
            return $@"create table {tableName} (
                LongVal bigint,
                IntVal int,
                ByteVal smallint,
                BoolVal boolean,
                ShortVal smallint,
                DecimalVal decimal(5, 2),
                DateTimeOffsetVal timestamp,
                DateTimeVal timestamp,
                StringVal varchar(64),
                GuidVal uuid
            )";
        }
    }
}