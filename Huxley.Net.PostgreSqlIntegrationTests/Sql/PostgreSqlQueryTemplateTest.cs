﻿using System;
using System.Data;
using Huxley.Net.SqlIntegrationTests.Sql;
using Npgsql;
using NUnit.Framework;

namespace Huxley.Net.PostgreSqlIntegrationTests.Sql {
    [TestFixture]
    public class PostgreSqlQueryTemplateTest : QueryTemplateTest {
        protected override string GetConnectionString() {
            return PostgreSqlConnectionHelper.ConnectionString;
        }

        protected override IDbConnection OpenConnection(string connectionString) {
            return new NpgsqlConnection(connectionString);
        }

        protected override string GetCreateTableSql(string tableName) {
            return $@"create table {tableName} (
                Id int,
                Name VARCHAR(16) NOT NULL unique,
                Value VARCHAR(64) NOT NULL,
                PRIMARY KEY(Id)
            )";
        }
    }
}